import datetime
import os

from flask import Flask, render_template, redirect, url_for
from forms import ItemForm
from models import Items
from database import db_session
from utils import flash_errors

app = Flask(__name__)
app.secret_key = os.environ['APP_SECRET_KEY']

@app.route("/", methods=('GET', 'POST'))
def add_item():
    form = ItemForm()
    if form.validate_on_submit():
        date_added=datetime.datetime.now()
        item = Items(name=form.name.data, quantity=form.quantity.data, description=form.description.data, date_added=date_added)
        try:
            db_session.add(item)
            db_session.commit()
            return redirect(url_for('success',item=form.name.data), code=307)
        except Exception as e:
            db_session.rollback()
            return redirect(url_for('failed'))
    else:
        flash_errors(form)
    return render_template('index.html', form=form)

@app.route("/success/<item>", methods=["POST"])
def success(item):
    return ("%s added succesfully" % item)

@app.route("/failed")
def failed():
     return "Failed to add new item. Please try again" 

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)